"use strict";

//PARTIE D
//const pizzName = 'Regina';
//const url =`images/${pizzName.toLowerCase()}.jpg`;
//console.log(url);
//const html = `<article class="pizzaThumbnail"><a href=\"${url}\"><img src=\"${url}\"/><section>Regina</section></a></article>
//`;
//console.log(html);
//document.querySelector('.pageContent').innerHTML = html;
//PARTIE E.1
//const data = ['Regina', 'Napolitaine', 'Spicy'];
//let html ="";
//for(let i=0;i<data.length;i++){
//let url =`images/${data[i].toLowerCase()}.jpg`;
// html += `<article class="pizzaThumbnail"> <a href="${url}"> <img src="${url}"/> <section>${data[i]}</section> </a> </article>`;
//console.log(html);
//}
//document.querySelector('.pageContent').innerHTML = html;

/*data.forEach(e => {
    let url =`images/${e.toLowerCase()}.jpg`;  
    html += `<article class="pizzaThumbnail">
     <a href="${url}"> <img src="${url}"/> 
     <section>${e}</section> 
     </a>
      </article>`;    
});
document.querySelector('.pageContent').innerHTML = html;*/
//PARTIE E.2
var data = [{
  name: 'Regina',
  base: 'tomate',
  price_small: 6.5,
  price_large: 9.95,
  image: 'https://images.unsplash.com/photo-1532246420286-127bcd803104?fit=crop&w=500&h=300'
}, {
  name: 'Napolitaine',
  base: 'tomate',
  price_small: 6.5,
  price_large: 8.95,
  image: 'https://images.unsplash.com/photo-1562707666-0ef112b353e0?&fit=crop&w=500&h=300'
}, {
  name: 'Spicy',
  base: 'crème',
  price_small: 5.5,
  price_large: 8,
  image: 'https://images.unsplash.com/photo-1458642849426-cfb724f15ef7?fit=crop&w=500&h=300'
}]; // trier par nom

/*data=data.sort(function(a,b){
    if ( a.name < b.name)
    return -1;
 if (a.name > b.name)
    return 1;

 return 0;
}  
);*/
//trier par prix petit format croissant

/*data=data.sort(function(a,b){
    if ( a.price_small < b.price_small)
    return -1;
 if (a.price_small > b.price_small)
    return 1;
        if(a.price_small == b.price_small){
            if ( a.price_large < b.price_large)
    return -1;
 if (a.price_large > b.price_large)
    return 1;
        }

 return 0;
}  
);*/
//filtrer par tomate

/*data = data.filter(function(e){
    if (e.base == 'tomate')
        return e;
});*/
//filtrer prix < 6

/*data = data.filter(function(e){
    if (e.price_small < 6)
        return e;
});*/
//filtrer le nom contient 2 fois i

/*function nbre_caracteres(lettre,mot)
    {
        mot2 = mot.split(lettre);
        nbre_de_fois_trouve = mot2.length-1;
        return nbre_de_fois_trouve;
    }

data = data.filter(function(e){
    if (nbre_caracteres('i',e.name)==2)
        return e;
});*/

var html = "";
data.forEach(function (e) {
  var url = "images/".concat(e.name.toLowerCase(), ".jpg");
  html += "<article class=\"pizzaThumbnail\">\n\t<a href=".concat(url, ">\n\t\t<img src=\"").concat(url, "\"/>\n        <section>\n            <h4>").concat(e.name, "</h4>\n            <ul>\n\t\t\t\t<li>Prix petit format : ").concat(e.price_small, " \u20AC</li>\n\t\t\t\t<li>Prix grand format : ").concat(e.price_large, " \u20AC</li>\n\t\t\t</ul>\n\n        </section>\n\t</a>\n</article>");
});
document.querySelector('.pageContent').innerHTML = html;
//# sourceMappingURL=main.js.map